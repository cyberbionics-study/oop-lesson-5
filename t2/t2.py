class Contact:
    """Base contact class"""
    def __init__(self, name, surname, age, mob_phone, email):
        self.surname = surname
        self.name = name
        self.age = age
        self.mob_phone = mob_phone
        self.email = email

    def get_contact(self):
        return f"{self.name} {self.surname}, {self.age} years old\nMobile Phone: {self.mob_phone}\nEmail: {self.email}"

    def sent_message(self, message):
        print(f"Message sent to {self.name} {self.surname}: {message}")


class UpdateContact(Contact):
    """Updated contact class"""
    def __init__(self, name, surname, age, mob_phone, email, job):
        super().__init__(name,surname,  age, mob_phone, email)
        self.job = job

    def get_message(self):
        return f"{self.name} {self.surname}'s job: {self.job}"


contact1 = Contact("John", "Smith" , 30, "123-456-7890", "john.smith@email.com")
update_contact1 = UpdateContact("Jane", "Doe", 25, "987-654-3210", "jane.doe@email.com", "Software Engineer")

# print("Contact 1:")
# print(contact1.__dict__)
# print("Base of Contact:", Contact.__base__)
# print("Bases of Contact:", Contact.__bases__)
#
# print("\nUpdateContact 1:")
# print(update_contact1.__dict__)
# print("Base of UpdateContact:", UpdateContact.__base__)
# print("Bases of UpdateContact:", UpdateContact.__bases__)
#
# print("\nContact 1 Info:")
# print(contact1.get_contact())
# contact1.sent_message("Hello, John!")
#
# print("\nUpdateContact 1 Info:")
# print(update_contact1.get_contact())
# print(update_contact1.get_message())
# update_contact1.sent_message("Hello, Jane!")
