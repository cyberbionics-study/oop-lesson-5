from t2 import t2


print(hasattr(t2.contact1, 'name'))
print(hasattr(t2.contact1, 'surname'))
print(hasattr(t2.contact1, 'age'))
print(hasattr(t2.contact1, 'email'))
print(hasattr(t2.contact1, 'mob_phone'))
print(hasattr(t2.contact1, 'job'))
print(hasattr(t2.update_contact1, 'job'))

print('')
print(getattr(t2.update_contact1, 'name'))
print(getattr(t2.update_contact1, 'surname'))
print(getattr(t2.update_contact1, 'age'))
print(getattr(t2.update_contact1, 'email'))
print(getattr(t2.update_contact1, 'mob_phone'))
print(getattr(t2.update_contact1, 'job'))
