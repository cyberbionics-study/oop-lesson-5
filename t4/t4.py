from t2 import t2


print(isinstance(t2.contact1, t2.Contact))
print(isinstance(t2.contact1, t2.UpdateContact))
print('')

print(isinstance(t2.update_contact1, t2.Contact))
print(isinstance(t2.update_contact1, t2.UpdateContact))
print('')

print(issubclass(t2.UpdateContact, t2.Contact))
print(issubclass(t2.Contact, t2.UpdateContact))


