from t2 import t2


print("Contact 1 Info:")
print(t2.contact1.get_contact())
t2.contact1.sent_message("Hello, John!")

print("\nUpdateContact 1 Info:")
print(t2.update_contact1.get_contact())
print(t2.update_contact1.get_message())
t2.update_contact1.sent_message("Hello, Jane!")

del t2.update_contact1.job

print("\nUpdateContact 1 Info (after deleting 'job'):")
print(t2.update_contact1.get_contact())
